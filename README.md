# Views Flag Refresh

Views Flag Refresh allows site administrators to configure which views are
refreshed automatically via AJAX when certain flags are selected. This is useful
when you have a view that filters by flagged content and you want the view to be
refreshed automatically when content is flagged or unflagged elsewhere on the
page.

The Views Flag Refresh module also allows administrators to select widgets that
alter the display of the view while it is being refreshed. Some examples are
displaying a throbber image or textual message alerting the user a refresh
action is being taken. By implementing the API, developers can add additional
widgets with ease. Refer to the [documentation](http://drupal.org/node/900924)
for more information.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_flag_refresh).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_flag_refresh).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the Flag module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Create a view
2. Add your fields including the Flag link field
3. In the OTHER section set "Use AJAX" option to Yes
4. Also in the OTHER section click the link next to "Flag refresh"
5. Select the current view
6. Click Apply
7. Clear cache
8. Now your view should refresh when you click on a flag link


## Maintainers

- Chris Pliakas - [cpliakas](https://www.drupal.org/u/cpliakas)
- Eugene Fidelin - [Eugene Fidelin](https://www.drupal.org/u/eugene-fidelin)
- Martin Anderson-Clutz - [mandclu](https://www.drupal.org/u/mandclu)
